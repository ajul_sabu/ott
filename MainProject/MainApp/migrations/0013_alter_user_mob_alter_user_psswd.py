# Generated by Django 4.0 on 2022-02-02 05:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('MainApp', '0012_rename_cu_program_ca'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='mob',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='psswd',
            field=models.IntegerField(null=True),
        ),
    ]
