# Generated by Django 4.0 on 2022-01-27 11:57

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('MainApp', '0006_alter_users_age'),
    ]

    operations = [
        migrations.CreateModel(
            name='category',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cname', models.CharField(max_length=40, null=True)),
                ('nos', models.IntegerField(null=True)),
                ('valid', models.IntegerField(null=True)),
                ('paid', models.BooleanField()),
                ('price', models.IntegerField(null=True)),
                ('image', models.ImageField(max_length=40, null=True, upload_to='')),
            ],
        ),
        migrations.CreateModel(
            name='program',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=40, null=True)),
                ('classes', models.IntegerField(null=True)),
                ('videourl', models.CharField(max_length=40, null=True)),
                ('image', models.ImageField(max_length=40, null=True, upload_to='')),
                ('cu', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='MainApp.users')),
            ],
        ),
    ]
